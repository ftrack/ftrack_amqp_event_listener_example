..
    :copyright: Copyright (c) 2017 ftrack

.. _using:

*****
Using
*****


Preperation
-----------

In order to consume events through a amqp queue a message broker must be installed
and ftrack configured.

You can choose to use either qpid ( https://qpid.apache.org/ ), which is bundled
with ftrack, or rabbitmq ( https://www.rabbitmq.com/ ). In the instructions below
we cover how to configure ftrack using rabbitmq running on the same host as ftrack.

Instructions should be valid for centos 6.

.. warning::

    Please try out the configuration on your staging server prior to deploying it to
    production.

.. note::

    This is just a simple installation example, you may need to tweak your
    limits, firwall and selinux settings for a production deployment.


Before installing RabbitMQ, you must install a supported version of Erlang/OTP.

* Update your yum repos::

    # In /etc/yum.repos.d/rabbitmq-erlang.repo

    [rabbitmq-erlang]
    name=rabbitmq-erlang
    baseurl=https://dl.bintray.com/rabbitmq/rpm/erlang/20/el/$releasever
    gpgcheck=1
    gpgkey=https://www.rabbitmq.com/rabbitmq-release-signing-key.asc
    repo_gpgcheck=0
    enabled=1

* Install erlang::

    yum install erlang

* Install the RabbitMq server::

    yum install https://github.com/rabbitmq/rabbitmq-server/releases/download/rabbitmq_v3_6_11/rabbitmq-server-3.6.11-1.el6.noarch.rpm

* Configure rabbit mq to listen on port 5673 on all insterfaces. We run it on the none default port 5673 to not interfere with qpid::

    # in /etc/rabbitmq/rabbitmq.config
    {rabbit, [
        {tcp_listeners, [{"0.0.0.0", 5673}]}
      ]}
    ].


* Setup the rabbitmq service::

    # Install the init script
    chkconfig rabbitmq-server on

    # start the rabbitmq service
    service rabbitmq-server start



* Next we configure ftrack to use rabbitmq::

    # in /opt/ftrack_config/ftrack.ini under [ftrack] section

    ftrack.enable_amqp_event_server = true
    ftrack.amqp_host = amqp://guest:guest@127.0.0.1:5673/%2f

Consuming
---------


Everything should now be in place for us to start consuming events, you can
verify your setup throguh the rabbit mq managment web interface, enabled with
the command::

    rabbitmq-plugins enable rabbitmq_management

Make sure the queue `ftrack-event-server-queue` exists and that exchanage
`ftrack-events`.


We can now create our own queue and bind it to the ftrack-events exchange
to receive our own copies of all internal ftrack events. Below is a simple
example were we create a new queue, bind it to the ftrack-events exchange
and print all events to stdout.

.. literalinclude:: resource/example_consumer.py

