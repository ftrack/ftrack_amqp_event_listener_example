..
    :copyright: Copyright (c) 2017 ftrack

.. _api_reference:

*************
ftrack_amqp_event_listener_example.consumer
*************

.. automodule:: ftrack_amqp_event_listener_example.consumer
    :members: