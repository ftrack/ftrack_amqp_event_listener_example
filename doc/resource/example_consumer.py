import os
import json
import pprint
import logging
import argparse

from ftrack_amqp_event_listener_example import (
    FTRACK_QUEUE_DEFAULTS
)

from ftrack_amqp_event_listener_example.consumer import (
    Consumer
)

logger = logging.getLogger(
    os.path.basename(
        __file__
    )
)

class EventConsumer(Consumer):
    '''Consumer that reads events.'''

    def on_message(self, unused_channel, basic_deliver, properties, body):
        '''Handle messages.'''
        super(EventConsumer, self).on_message(
            unused_channel, basic_deliver, properties, body
        )


        event = json.loads(
            body
        )

        pprint.pprint(
            event
        )


def main():
    parse = argparse.ArgumentParser(
        'event consumer example'
    )

    parse.add_argument(
        '-q', '--queue_name', default='example-consumer',
            help='name of queue to create / consume. deafault : %(default)'
    )

    parse.add_argument(
        '-b', '--broker', default='amqp://guest:guest@localhost:5672/%2f',
            help='broker. deafault : %(default) '
    )

    parse.add_argument(
        '-v', '--verbose', default=False, action='store_true'
    )


    args = parse.parse_args()

    logging.basicConfig(
        level=logging.INFO if args.verbose else logging.WARNING
    )

    # As a safety measure we set the queues message ttl to 24,
    # hours. This will prevent the queue from growing out of control
    # if there are no consumers available
    ttl = FTRACK_QUEUE_DEFAULTS.DEFAULT_TTL


    # The EventConsumer will, once started, create a new queue
    # bind it to the ftrack-events exchange and start consuming
    # messages.
    consumer = EventConsumer(
        args.broker,
        args.queue_name,
        FTRACK_QUEUE_DEFAULTS.DEFAULT_EXCHANGE,
        FTRACK_QUEUE_DEFAULTS.DEFAULT_EXCHANGE_TYPE,
        FTRACK_QUEUE_DEFAULTS.DEFAULT_ROUTING_KEY,
        ttl
    )

    try:
        logger.warning(
            'Starting to consume.'
        )

        consumer.run()
    except KeyboardInterrupt:
        logger.warning(
            'Stopping..'
        )

        consumer.stop()

    logger.warning(
        'Finished.'
    )

if __name__ == '__main__':
    main()