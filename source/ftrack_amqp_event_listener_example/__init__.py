# :coding: utf-8
# :copyright: Copyright (c) 2017 ftrack

from ._version import (
    __version__
)

from ftrack_amqp_event_listener_example.consumer import (
    Consumer
)


class FTRACK_QUEUE_DEFAULTS:
    DEFAULT_TTL = 3600 * 24 * 1000 # 24 hours in milliseconds
    DEFAULT_EXCHANGE = 'ftrack-events'
    DEFAULT_EXCHANGE_TYPE = 'topic'
    DEFAULT_ROUTING_KEY = '#'


